const fig1 = {
    number: 1,
    name: "The Square",
    tune: 'reels',
    bars: 128,
    elements: [
        {
            name: 'Lead around',
            bars: 8,
            couples: "all",
            description : `partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies under both arms into waltz hold on the last 2 bars.`
        },
        {
            name: 'Swing',
            bars: 8,
            couples: "2 sides",
            description : `in waltz hold`
        }
    ]
};

export default fig1;