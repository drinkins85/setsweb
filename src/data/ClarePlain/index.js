

const ClarePlain = {
    id: 2,
    code: 'clareplain',
    name: "Clare Plain",
    figures: [
        {
            number: 2,
            name: "Turn the Lady",
            tune: 'reels',
            bars: 128,
            elements: [
                {
                    name: 'Lead around',
                    bars: 8,
                    couples: "all",
                    description: `partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies under both arms into waltz hold on the last 2 bars.`
                },
                {
                    name: 'Swing',
                    bars: 8,
                    couples: "sides",
                    description: `in waltz hold`
                },
                {
                    name: 'House inside',
                    bars: 8,
                    couples: "1 top",
                    description: `1st top couple house within the set`
                }
            ]
        },
        {
            number: 3,
            name: "Wheels",
            tune: 'jigs',
            bars: 96,
            elements: [
                {
                    name: 'Lead around',
                    bars: 8,
                    couples: "all",
                    description: `partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies under both arms into waltz hold on the last 2 bars.`
                },
                {
                    name: 'Swing',
                    bars: 8,
                    couples: "all",
                    description: `in waltz hold`
                },
                {
                    name: 'House inside',
                    bars: 8,
                    couples: "2 top",
                    description: `1st top couple house within the set`
                }
            ]
        },
        {
            number: 4,
            name: "Wheels",
            tune: 'polkas',
            bars: 246,
            elements: [
                {
                    name: 'Lead around',
                    bars: 8,
                    couples: "all",
                    description: `partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies under both arms into waltz hold on the last 2 bars.`
                },
                {
                    name: 'Swing',
                    bars: 8,
                    couples: "tops",
                    description: `in waltz hold`
                },
                {
                    name: 'House inside',
                    bars: 8,
                    couples: "1 top",
                    description: `1st top couple house within the set`
                }
            ]
        }
    ]

};

export default ClarePlain;