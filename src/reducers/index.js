import { combineReducers } from 'redux';
import sets from './sets';
import config from './config';


export default combineReducers({
    sets: sets,
    config: config
})