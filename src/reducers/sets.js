const initialState = [];

export default function sets(state = initialState, action){
    switch (action.type) {
        case 'LOAD_SETS_DATA_SUCCESS':
            return action.payload;
        case 'ADD_SET':
            return [...state, action.payload];
        case 'EDIT_SET':
            return state.map(set => {
                if (set.code === action.payload.code){
                    set.name = action.payload.name;
                    set.figures = action.payload.figures;

                }
                return set
            });
        case 'DELETE_SET':
            return state.filter(set => set.id !== action.payload);
        case 'SETS_FILTER':
            if (action.payload === ''){
                return state;
            }
            let filtredSets = [];
            state.map(set => {
                let filtredFigures = set.figures.filter(figure => figure.tune === action.payload);
                if (filtredFigures.length >0){
                    let newset = {
                        code: set.code,
                        name: set.name,
                        figures: filtredFigures
                    };
                     filtredSets.push(newset)
                }
            });
            return filtredSets;
        default:
            return state;
    }
}