const initialState = {};

export default function config(state = initialState, action){
    switch (action.type) {
        case 'LOAD_CONFIG_DATA_SUCCESS':
            return action.payload;
        default:
            return state;
    }
}