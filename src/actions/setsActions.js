
export function loadData(){
    return dispatch => {

        const readApi = 'http://sets.drinkins.com/rest/read.php';
        fetch(readApi, {
            method: 'GET',
            headers: {
                'Authorization': 'mRyaW5raW5zLmNvbPep5UeQZULUrcB3uOs'
            }
        })
            .then(res => {
                if (res.status === 200){
                    return res.text()
                } else {
                    throw new Error(res.status)
                }
            })
            .then(data => {

                // BOM
                let sets = data.slice(1);
                if (sets !== 'empty'){
                    return JSON.parse(stripslashes(sets));
                } else {
                    return null;
                }
                //console.log("get data= ", prs);
            })
            .then(sets => {
                if (!sets){
                    sets = [{name:"No sets found", figures:[]}];
                }
                dispatch({type: 'LOAD_SETS_DATA_SUCCESS', payload: sets})

            })
            .catch(err => console.error(err));

       // setTimeout(()=> dispatch({type: 'LOAD_SETS_DATA_SUCCESS', payload: data}),1000)

    }
}

export function addSet(set) {
    return dispatch => {
        const addApi = 'http://sets.drinkins.com/rest/add.php';
        const getIdApi = 'http://sets.drinkins.com/rest/getid.php';

     //   console.log("try add new set",JSON.stringify(set));

        fetch(addApi, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionStorage.getItem('key')}`
            },
            body: JSON.stringify(set)
        })
            .then((res => {
                if (res.status === 200){
                    return res.text()
                } else {
                    throw new Error(res.status)
                }
            }))
            .then(data => {
                console.log(data);

                fetch(getIdApi, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: set.code
                }).then(res => res.text())
                    .then(id => {
                        set.id = +id;
                        dispatch({type: 'ADD_SET', payload: set})
                    })
                    .catch(err => console.error(err));

            })

               // dispatch({type: 'ADD_SET', payload: set})})
            .catch(err => console.error(err));



        /*store.put('classrooms2', JSON.stringify(newClassroom))
            .then(() => {
                dispatch({type: 'ADD_CLASSROOM', payload: newClassroom})
            }); */
    }
}

export function editSet(set) {
    return dispatch => {
        const editApi = 'http://sets.drinkins.com/rest/edit.php';
        fetch(editApi, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionStorage.getItem('key')}`
            },
            body: JSON.stringify(set)
        })
            .then((res => {
                if (res.status === 200){
                    return res.text()
                } else {
                    throw new Error(res.status)
                }
            }))
            .then(data => {
                console.log(data);
                dispatch({type: 'EDIT_SET', payload: set})
            })
            .catch(err => console.error(err));

    }
}

export function deleteSet(id) {
    return dispatch => {

        const editApi = 'http://sets.drinkins.com/rest/delete.php';
        fetch(editApi, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionStorage.getItem('key')}`
            },
            body: id
        })
            .then((res => {
                if (res.status === 200){
                    return res.text()
                } else {
                    throw new Error(res.status)
                }
            }))
            .then(data => {
                console.log(data);
                dispatch({type: 'DELETE_SET', payload: id})
            })
            .catch(err => console.error(err));



       /* store.delete('classrooms2', id)
            .then(() => {
                dispatch({type: 'DELETE_CLASSROOM', payload: id})
            }); */
    }
}

export function filterByTune(filter) {
    return dispatch => {
        console.log(filter);
        dispatch({type: 'SETS_FILTER', payload: filter});
    }
}


function stripslashes(str) {
    str = str.replace(/\\'/g, '\'');
    str = str.replace(/\\"/g, '"');
    str = str.replace(/\\0/g, '\0');
    str = str.replace(/\\\\/g, '\\');
    return str;
}

