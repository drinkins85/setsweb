import React from 'react'
import { Form, Text, Select, FormError, Textarea } from 'react-form'
import '../../css/form.css';

class EditSetForm extends React.Component {

    render() {
        return (
            <div className="form-add-set-container">
                <h3 className="header-figure">Edit Set</h3>
                <Form
                    onSubmit={(values) => {
                      //  console.log(values);
                        this.props.onEdit(values);
                        this.props.onFinish();
                    }}

                    defaultValues={this.props.set}

                    validate={(values) => {

                        const { name, code, figures } = values;
                        return {
                            name: !name ? 'A set name is required' : undefined,
                            code: !code ? 'A set code is required' : undefined,
                            figures: (!figures || !figures.length) ? 'You need at least one figure' : figures.map((figure,n) => {
                                const { name, number, tune, bars } = figure;
                                return {
                                    name: !name ? 'A figure name is required' : undefined,
                                    number: !number ? 'A figure number is required' : undefined,
                                    tune: !tune ? 'A figure tune is required' : undefined,
                                    bars: !bars ? 'A bars is required' : undefined,
                                    elements: (!figures[n].elements || !figures[n].elements.length)
                                        ? 'You need at least one element'
                                        : figures[n].elements.map(element =>{
                                            const { name, couples, bars } = element;
                                            return {
                                                name: !name ? 'A element name is required' : undefined,
                                                bars: !bars ? 'A bars is required' : undefined,
                                                couples: !couples ? 'A couples is required' : undefined,

                                            }
                                        })


                                }
                            })
                        }
                    }}
                >
                    {({values, submitForm, addValue, removeValue, setValue, resetForm, swapValues}) => {
                        return (
                            <form onSubmit={submitForm} className="form-add-set">
                                <Text field='name'  placeholder='Set name' className="input_size_xl" />
                                <Text field='code'  placeholder='Set code' />
                                <FormError field='figures' />
                                <div className="form-add-set__figures" >
                                    {!values.figures.length ? (
                                        <em>No figures have been added yet</em>
                                    ) : values.figures.map((figure, i) => (
                                        <div key={i} className="form-add-set__figure">

                                            <button className="btn_remove" type='button' onClick={() => window.confirm("Delete figure?") ? removeValue('figures', i) : false }>
                                                X
                                            </button>

                                            <Text field={['figures', i, 'number']} placeholder='Figure Number' className="input_size_s"  />

                                            <Text field={['figures', i, 'name']}  placeholder='Figure Name'  className="input_size_xl" />

                                            <label>Tune</label>
                                            <Select
                                                field={`figures.${i}.tune`}
                                                options={[{
                                                    label: 'Reels',
                                                    value: 'reels'
                                                }, {
                                                    label: 'Jigs',
                                                    value: 'jigs'
                                                }, {
                                                    label: 'Polkas',
                                                    value: 'polkas'
                                                }, {
                                                    label: 'Slides',
                                                    value: 'slides'
                                                }, {
                                                    label: 'Hornpipes',
                                                    value: 'hornpipes'
                                                }, {
                                                    label: 'Flings',
                                                    value: 'flings'
                                                }]}
                                            />

                                            <Text field={['figures', i, 'bars']}  placeholder='bars' className="input_size_s"  />
                                            <FormError field={`figures.${i}.elements`} />
                                            <div className="form-add-set__elements">

                                                {
                                                    !figure.elements.length ? (
                                                    <em>No elements have been added yet</em>
                                                    ) : figure.elements.map((element, j) => (
                                                        <div key={j} className="form-add-set__element">

                                                            <button className="btn_remove"
                                                                    type='button'
                                                                    onClick={() => window.confirm("Delete element?") ? removeValue(`figures.${i}.elements`, j) : false }>
                                                                X
                                                            </button>

                                                            {
                                                                j > 0 ?
                                                                    <button className="btn_up" type="button" onClick={() => swapValues(`figures.${i}.elements`, j, j - 1)}>
                                                                        &uarr;
                                                                    </button>
                                                                : false
                                                            }

                                                            {
                                                                j < figure.elements.length-1 ?
                                                                    <button className="btn_down" type="button" onClick={() => swapValues(`figures.${i}.elements`, j, j + 1)}>
                                                                        &darr;
                                                                    </button>
                                                                : false
                                                            }


                                                            <br/>


                                                            <Text className="input_size_xl" field={[`figures.${i}.elements`, j, 'name']} placeholder='Element name'  />



                                                            <Select
                                                                className="input_size_xl"
                                                                field={`figures.${i}.elements.${j}.couples`}
                                                                options={[{
                                                                    label: 'All couples',
                                                                    value: 'all'
                                                                }, {
                                                                    label: 'Tops',
                                                                    value: 'tops'
                                                                }, {
                                                                    label: 'Sides',
                                                                    value: 'sides'
                                                                }, {
                                                                    label: '1st top couple',
                                                                    value: '1 top'
                                                                }, {
                                                                    label: '2nd top couple',
                                                                    value: '2 top'
                                                                }, {
                                                                    label: '1st side couple',
                                                                    value: '1 side'
                                                                }, {
                                                                    label: '2nd side couple',
                                                                    value: '2 side'
                                                                }, {
                                                                    label: 'Ladies',
                                                                    value: 'ladies'
                                                                }, {
                                                                    label: 'Gents',
                                                                    value: 'gents'
                                                                }]}
                                                            />

                                                            <Text className="input_size_s" field={[`figures.${i}.elements`, j, 'bars']} placeholder='Bars'  />

                                                            <div className="ta-container">
                                                                    <Textarea
                                                                        className="description"
                                                                        field={[`figures.${i}.elements`, j, 'description']}
                                                                        placeholder='Description'
                                                                    />
                                                            </div>

                                                        </div>
                                                    ))
                                                }

                                                <div>
                                                    <button className="btn_add" type='button' onClick={() => addValue(`figures.${i}.elements`, {})}>
                                                        Add Element
                                                    </button>
                                                </div>
                                            </div>

                                            <br/>

                                            <Text   field={`figures.${i}.playlist`}  placeholder='Youtube plylist ID' />
                                            <Text   field={`figures.${i}.music`}  placeholder='music link' />




                                        </div>
                                    ))}

                                    <div>
                                        <button className="btn_add" type='button' onClick={() => addValue('figures', {elements:[]})}>
                                            Add Figure
                                        </button>
                                    </div>


                                </div>





                                <button className="btn_submit" type='submit'>Save</button>

                            </form>



                        )
                    }}
                </Form>
            </div>
        )
    }

}

export default EditSetForm;

