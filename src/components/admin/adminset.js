import React from 'react';
import EditSetForm from './editsetform'

class AdminSet extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            isEditing:false
        };

        this.hideForm = this.hideForm.bind(this);

    }

    hideForm(){
        this.setState({ isEditing:false})
    }

    renderForm(){
        return(
            <div>
            <button className="btn_remove" onClick={this.hideForm}>X</button>
            <EditSetForm set={this.props.set} onEdit={this.props.onEdit} onFinish={this.hideForm} />

            </div>
        )
    };

    renderDisplay(){
        return(
            <div className="flex-row " >
                <div className="col_m ">
                    <div className="col_m nopadding">{this.props.set.id}</div>
                </div>
                <div className="flex-link col_xl nopadding">
                    <div className="col_xl margin-left">{this.props.set.name}</div>
                </div>

                <div className="col_m ">
                    <button onClick={()=>this.setState({ isEditing:true})}>Edit</button>
                </div>
                <div className="col_m ">
                    <div className="col_m nopadding">{this.props.set.statistic}</div>
                </div>
                <div className="col_m">
                    <button onClick={()=>{ window.confirm("Delete set?") ? this.props.onDelete(this.props.set.id): false}}>Delete</button>
                </div>

            </div>
        )
    }

    render(){
        return (this.state.isEditing ? this.renderForm() : this.renderDisplay());
    }

}

export default AdminSet;