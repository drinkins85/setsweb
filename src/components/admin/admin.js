import React from 'react';
import {NavLink} from 'react-router-dom';
import AddSetForm from './addsetform';
import AdminSet from './adminset';
import LoginForm from '../login';

class Admin extends React.Component{

    constructor(props){
        super(props);
       // console.log(props);
    }

    checkLocalStorageKey(){
        return !!sessionStorage.getItem('key');
    }



    render(){
        return(
            <div>
                {
                    this.props.sets.length === 0 ?
                        <div>
                            <span>Loading</span>
                        </div>
                        : this.checkLocalStorageKey() ?
                            <div>
                                <div className="breadcrumbs">
                                    <NavLink className="breadcrumbs__item" to="/">Setlist</NavLink>
                                </div>
                                <h2 className="header-set">Setlist</h2>
                                {
                                    this.props.sets.map((set, it) => {
                                        return (
                                            <AdminSet set={set}
                                                      key={it}
                                                      onEdit={this.props.setsactions.editSet}
                                                      onDelete={this.props.setsactions.deleteSet}
                                            />
                                        )
                                    })
                                }
                                <AddSetForm onAdd={this.props.setsactions.addSet}/>
                            </div>
                        : <LoginForm/>
                }
            </div>
        )
    }

}

export default Admin;


function stripslashes(str) {
    str = str.replace(/\\'/g, '\'');
    str = str.replace(/\\"/g, '"');
    str = str.replace(/\\0/g, '\0');
    str = str.replace(/\\\\/g, '\\');
    return str;
}


/*

 <div className="flex-row " key={it}>
 <div className="flex-link col_xl nopadding">
 <div className="col_xl margin-left">{set.name}</div>
 </div>
 <NavLink className="flex-link col_m nopadding" to={`/${set.code}`}>
 <div className="col_m">Edit</div>
 </NavLink>
 </div>
 */

/*
*
* figures: [
 {
 number: 2,
 name: "Turn the Lady",
 tune: 'hornpipes',
 bars: 128,
 elements: [
 {
 name: 'Lead around',
 bars: 8,
 couples: "all",
 description: `partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies under both arms into waltz hold on the last 2 bars. partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies partners take crossed hand hold in front and dance around anti-clockwise to place, the gents turning the ladies`
 },
 {
 name: 'Swing',
 bars: 8,
 couples: "sides",
 description: `in waltz hold`
 },
 {
 name: 'House inside',
 bars: 8,
 couples: "1 top",
 description: `1st top couple house within the set`
 }
 ]
 }
 ]
* */