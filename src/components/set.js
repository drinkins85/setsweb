import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Figures from './figures';
import Figure from './figure';

class Set extends React.Component{


    getSet(){
        if (this.props.sets.length > 0){

            for (let i=0; i<this.props.sets.length; i++){
                if (this.props.sets[i].code === this.props.match.params.code){
                    return this.props.sets[i];
                }
            }}
        return false;
    }

    render(){

        let set = false;

        if (this.props.sets.length > 0){
            set =  this.getSet();
        }


        //console.log("PROPS", this.props);

        return(

            this.props.sets.length === 0 ?
                <div>
                    <span>Loading</span>
                </div>
                :
                set ?
                    <div>
                        <Switch>
                            <Route exact path='/:code'
                                   render={() => <Figures figures={set.figures} setname={set.name}
                                                          setcode={set.code}/> }/>
                            <Route path='/:code/:number'
                                   render={(props) => <Figure figures={set.figures} {...props}
                                                              setname={set.name} setcode={set.code}/> }/>
                        </Switch>
                    </div>
                    :
                    <div>
                        404
                    </div>


        )
    }
}

export default Set;

