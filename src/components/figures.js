import React from 'react';
import {NavLink} from 'react-router-dom';
import Helmet from 'react-helmet';

class Figures extends React.Component{
    render(){
        return(
            <div>
                <Helmet
                    title={`${this.props.setname} Set`}
                    meta={[
                        {"name": "description", "content": `${this.props.setname} Set`},
                        {"name": "keywords", "content": `${this.props.setname} set, figures`}
                    ]}
                />
                <div className="breadcrumbs">
                     <NavLink className="breadcrumbs__item" to="/">Setlist</NavLink>
                     <NavLink className="breadcrumbs__item" to={`/${this.props.setcode}`}>{this.props.setname}</NavLink>
                </div>
                <h2 className="header-set">{this.props.setname}</h2>
                {
                    this.props.figures.map( (figure,it) => {
                        return(
                                <NavLink className="flex-row flex-link" key={it}  to={`${this.props.setcode}/${figure.number}`} >
                                    <div className="col_m margin-right font-size_m ">fig. {figure.number}</div>
                                    <div className="col_xl">{figure.name}</div>
                                    <div className="col_m font-size_m ">{figure.tune}</div>
                                    <div className="col_l font-size_m ">{figure.bars} bars</div>
                                </NavLink>
                        )
                    })
                }
            </div>
        )
    }
}

export default Figures;