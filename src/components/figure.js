import React from 'react';
import {NavLink} from 'react-router-dom';
import Helmet from 'react-helmet';
import Youtube from './youtube';

class Figure extends React.Component{

    getFigure(){
        if (this.props.figures.length > 0){
            for (let i=0; i<this.props.figures.length; i++){
            //    console.log(this.props.figures[i].number, " = ", this.props.match.params.number);
                if (+this.props.figures[i].number === +this.props.match.params.number){
                    return this.props.figures[i];
                }
            }}
     //   console.log("get return false");
        return false;
    }

    render(){

        let figure = false;

        if (this.props.figures.length > 0){
            figure =  this.getFigure();
          //  console.log("getting")
        }
      //  console.log(figure);
        return (
            this.props.figures.length === 0 ?
                <div>
                    <span>Loading</span>
                </div>
                :
                figure ?
                    <div>
                        <Helmet
                            title={`${this.props.setname} Set fig. ${figure.number}`}
                            meta={[
                                {"name": "description", "content": `${this.props.setname} Set figure ${figure.number}`},
                                {"name": "keywords", "content": `${this.props.setname} set, fig. ${figure.number}, ${figure.name}`}
                            ]}
                        />
                        <div className="breadcrumbs">
                            <NavLink className="breadcrumbs__item" to="/">Setlist</NavLink>
                            <NavLink className="breadcrumbs__item" to={`/${this.props.setcode}`}>{this.props.setname}</NavLink>
                            <NavLink className="breadcrumbs__item" to={`/${this.props.setcode}/${figure.number}`}>fig. {figure.number}</NavLink>
                        </div>
                        <h2 className="header-set">{this.props.setname} <nobr>{figure.number} figure</nobr></h2>
                        <div className="flex-row bgcolor_orange_light">
                            <div className="col_l bgcolor_orange_mid">
                                fig. {figure.number}
                            </div>
                            <div className="col_xl nopadding ">
                                <h3 className="header-figure">{figure.name}</h3>
                            </div>
                            <div className="col_l">
                                {figure.tune}
                            </div>
                            <div className="col_l">
                                {figure.bars} bars
                            </div>
                        </div>
                        <div >
                            {
                                figure.elements.map((element,it) => {
                                    return(
                                        <div key={it}>
                                            <div className="flex-row border_b" >
                                                <div className={`col_m col_couples color_cheme_${element.couples.replace(" ", "-")}`}>
                                                   {element.couples}
                                                </div>
                                                <div className="col_xl col_element">
                                                    <span className="header-element">{element.name}</span>
                                                </div>

                                                <div className={`col_xxl ${!element.description ? 'hfm' : false}`}>
                                                    <div className="element-description">{element.description}</div>
                                                </div>

                                                <div className="col_m col_bars">{element.bars}</div>
                                            </div>

                                        </div>
                                    )
                                })
                            }
                        </div>



                        <div className="video">
                            <h3>{`Watch ${this.props.setname} fig ${figure.number} on Youtube`} </h3>
                            <Youtube playlistId={figure.playlist} setname={this.props.setname} figure={figure.number}/>
                        </div>

                    </div>
                    :
                    <div>
                        404.
                    </div>
        )
    }

}

export default Figure;

/*
 <div key={it}>
 <div className="flex-row" >
 <div className={`col_m color_cheme_${element.couples}`}>{element.couples}</div>
 <div className="col_xl">
 <h4 className="header-element">{element.name}</h4>
 </div>
 <div className="col_m">{element.bars}</div>
 </div>
 <div className="flex-row border_b " >
 <div className="col_xl element-description">{element.description}</div>
 </div>
 </div>
 * */