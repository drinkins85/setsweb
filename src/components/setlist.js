import React from 'react';
import {NavLink} from 'react-router-dom';
import Helmet from 'react-helmet';

class SetList extends React.Component{

    constructor(props){
        super(props);
        this.filterByTune = this.filterByTune.bind(this);
        this.state = {
            isFiltred: false
        }
    }

    filterByTune(){
        this.setState({isFiltred:true});
        let filter = this.refs.tunes.value;
        let allFigures = document.querySelectorAll(".figure");
        if (filter === 'reset'){
            for (let i=0; i< allFigures.length; i++){
                allFigures[i].classList.remove('filtred');
                this.setState({isFiltred:false});
            }
        } else {
            for (let i=0; i< allFigures.length; i++){
                allFigures[i].classList.add('filtred');
                if (allFigures[i].classList.contains(filter)){
                    allFigures[i].classList.remove('filtred');
                }
            }
        }
    }

    render(){
        return(
            <div>
                <Helmet
                    title="Set dances"
                    meta={[
                        {"name": "description", "content": "List of set dances"},
                        {"name": "keywords", "content": "sets, setdances, setdancing"}
                    ]}
                />
                {
                    this.props.sets.length === 0 ?
                        <div>
                            <span>Loading</span>
                        </div>
                        :
                        <div>
                            <div className="flex-row " >

                                <div className="col_xl nopadding">
                                    <div className="col_xl margin-left"><h2 className="header-set">Setlist</h2></div>
                                </div>
                                <div className="col_xm text-right">
                                    <div className="col_xm nopadding text-right">
                                        <select onChange={this.filterByTune} ref="tunes">
                                            {
                                                !this.state.isFiltred ?  <option default value="">Filter by tune</option>
                                                    : <option value="reset">Reset filter</option>

                                            }
                                            <option value="reels">Reels</option>
                                            <option value="jigs">Jigs</option>
                                            <option value="polkas">Polkas</option>
                                            <option value="slides">Slides</option>
                                            <option value="hornpipes">Hornpipes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            {
                                this.props.sets.map((set, it) => {
                                    return (
                                        <div className="flex-row " key={it}>
                                            <NavLink className="flex-link col_xl nopadding" to={`/${set.code}`}>
                                                <div className="col_xl margin-left">{set.name}</div>
                                            </NavLink>
                                            {
                                                set.figures.map((figure, iterator) => {
                                                    return (
                                                        <NavLink key={`f_${iterator}`}
                                                                 className="flex-link col_l nopadding"
                                                                 to={`/${set.code}/${figure.number}`}>
                                                            <div className={`col_l font-size_m pad_10 figure ${figure.tune}`}>
                                                                <nobr><span
                                                                    className="setlist-fig">fig.</span> {figure.number}
                                                                </nobr>
                                                                <br/>{figure.tune}
                                                               <span className="list-bars"> {figure.bars}</span>
                                                            </div>
                                                        </NavLink>
                                                    )
                                                })
                                            }

                                        </div>
                                    )
                                })}
                        </div>
                }
            </div>
        )
    }

}

export default SetList;