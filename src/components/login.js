import React from 'react';
import {Redirect} from 'react-router-dom';

// add in .htaccess for CGI mode
// SetEnvIf Authorization .+ HTTP_AUTHORIZATION=$0

class loginForm extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            login: '',
            password: '',
            status:'',
            redirect: false
        };

        this.onSubmit = this.onSubmit.bind(this);
        //this.stuff = this.stuff.bind(this);

    }

    onSubmit(event){
        event.preventDefault();
        //console.log(this.refs.login.value, this.refs.password.value);
        this.setState({status: 'loading' });

        const authApi = 'http://sets.drinkins.com/auth/auth.php';
        const credentials = {
            login: this.refs.login.value,
            password: this.refs.password.value
        };

        fetch(authApi, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({auth: credentials})
        })
            .then(res => {
                if (res.status === 200)
                {
                    return res.text()
                } else {
                    this.setState({status: ''});
                    throw new Error(res.status, res.statusText);
                }
            })
            .then(key => {
               // this.setState({key: key});
               // console.log("key saved");
                sessionStorage.setItem('key', key)
            })
            .then( () => {
                this.setState({redirect: true});
            } )
            .catch(error => alert(error));
    }
/*
    stuff(){
        if (sessionStorage.getItem('key')) {
            const testApi = 'http://sets.drinkins.com/auth/check.php';
            fetch(testApi, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${sessionStorage.getItem('key')}`
                }
            })
                .then(res => {
                    if (res.status === 200){
                        return true;
                    } else {
                        return false;
                    }
                   // res.text()
                })

        } else {
            return false;
        }
    } */

    render(){
        return(
            !this.state.redirect ?
                <div>
                    <form onSubmit={this.onSubmit}>
                        <input type="text" name="login" placeholder="Login" ref="login"/>
                        <input type="password" name="password" placeholder="Password" ref="password"/>
                        <button type="submit" className="btn_add">{this.state.status === 'loading' ? "loading.." : "Authorize"}</button>
                    </form>
                </div>
            :
                <Redirect push to="/admin"/>

        )
    }


}

export default loginForm;