import React from 'react'

class Youtube extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            playlistId: this.props.playlistId
        }
    }

    render(){
        const playlistUrl = `https://www.youtube.com/embed/videoseries?list=${this.state.playlistId}`;
        return(
            this.state.playlistId ?
                <div className="youtube-video">
                    <iframe width="560" height="315" src={playlistUrl} frameborder="0" allowfullscreen></iframe>
                </div>
            :
                <div className="youtube-video">
                    <iframe width="560" height="315" src={`http://www.youtube.com/embed?listType=search&list=${this.props.setname} set ${this.props.figure} figure`} frameborder="0" allowfullscreen></iframe>
                </div>
        )
    }

}

export default  Youtube;