import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as setsActions from './actions/setsActions';
import * as configActions from './actions/configActions';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import SetList from './components/setlist';
import Set from './components/set';
import Admin from './components/admin/admin';
import LoginForm from './components/login';


class App extends React.Component {

    constructor(props) {
        super(props);
        this.props.setsActions.loadData();
        this.props.configActions.loadSettings();
    }

    render(){
        //console.log("PROPS_",this.props);
        return (
            <div className="mainContainer">
                <div className="header">
                    <span className="logo">SetsApp</span>
                </div>

                <div className="content">
                    <Router>
                        <Switch>
                            <Route exact path='/' render={() => <SetList sets={this.props.sets} /> }/>
                            <Route path='/admin' render={() => <Admin sets={this.props.sets} setsactions={this.props.setsActions} /> }/>
                            <Route path='/login' render={(props) => <LoginForm {...props} /> }/>
                            <Route path='/:code' render={(props) => <Set sets={this.props.sets} {...props} /> }  />
                            <Route path='*' render={() => {
                                return <div className="page-not-found"><h1>Page not found</h1></div>
                                }
                            }/>
                        </Switch>
                    </Router>
                </div>
            </div>
        )
    }

}

function mapStateToProps (state) {
    return {
        sets: state.sets,
        config: state.config
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setsActions: bindActionCreators(setsActions, dispatch),
        configActions: bindActionCreators(configActions, dispatch),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);